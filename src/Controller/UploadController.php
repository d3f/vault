<?php
/**
 * Created by PhpStorm.
 * User: Estaban
 * Date: 13-12-2017
 * Time: 14:32
 */

namespace App\Controller;


use App\Form\Models\Upload;
use App\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class UploadController extends Controller {

	/**
	 * @Route(path="/upload/", name="upload")
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function uploadAction(Request $request) {
		$storage = $this->get( 'app.storage.aws' );
		$model = new Upload();
		$form = $this->createForm( UploadType::class, $model );

		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ){
		    $file = $request->files->get('upload');
		    if($file['file']->getError()){
		        var_dump($file['file']->getError());exit;
            }
			$storage->uploadFile($file['file'],$model->getVisibility());
		    #TODO: replace user 1 with actual user system
            $storage->addToDatabase(['file' => $file['file']->getClientOriginalName(),'type' => $file['file']->getMimeType(),'user' => 1,'name' => $model->getName(),'visibility' => $model->getVisibility()]);
			return $this->redirectToRoute( 'uploadSuccess' );
		}

		return $this->render('upload/upload.html.twig', array(
			'form' => $form->createView(),
		));
	}

	/**
	 * @Route(path="/upload/success/",name="uploadSuccess")
	 *
	 * @return Response
	 */
	public function uploadSuccessAction(){

		return $this->render('upload/uploadSuccess.html.twig', array(
		));
	}
}