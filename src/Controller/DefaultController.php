<?php
/**
 * Created by PhpStorm.
 * User: Estaban
 * Date: 13-12-2017
 * Time: 14:22
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller {

	/**
	 * @Route(path="/", name="index")
	 *
	 * @return Response
	 */
	public function indexAction() {
		return $this->render('default/index.html.twig');
	}}