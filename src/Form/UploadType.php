<?php
/**
 * Created by PhpStorm.
 * User: Estaban
 * Date: 13-12-2017
 * Time: 14:39
 */

namespace App\Form;


use App\Form\Models\Upload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'label' => 'File',
                'attr' => ['placeholder' => 'Url', 'class' => 'form-control'],
                'label_attr' => ['class' => 'col-lg-2 control-label']
            ])
            ->add('name', TextType::class,[
                'label' => 'Name',
                'attr' => ['placeholder' => 'Name', 'class' => 'form-control'],
                'label_attr' => ['class' => 'col-lg-2 control-label']
            ])
            ->add('visibility', ChoiceType::class, [
                'label' => 'Visibility',
                'choices' => [
                    'Public' => 'public-read',
                    'Private' => 'private',
                    'Group' => 'group'
                ],
                'attr' => ['class' => 'form-control'],
                'label_attr' => ['class' => 'col-lg-2 control-label']
            ])
            ->add('save', SubmitType::class, ['label' => 'Upload'])
            ->getForm();
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                                   'data_class' => Upload::class,
                               ));
    }
}