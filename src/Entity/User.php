<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false, name="user_name")
     */
    private $userName;

    /**
     * @ORM\Column(type="string",length=255, unique=false, nullable=false, name="password")
     */
    private $password;

    /**
     * @ORM\Column(type="string",length=255, unique=true,nullable=false, name="email")
     */
    private $email;

    /**
     * @ORM\Column(type="string",length=50,unique=false, nullable=true, name="role")
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Upload", mappedBy="user", cascade={"detach"})
     */
    private $uploads;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getUploads()
    {
        return $this->uploads;
    }

    /**
     * @param mixed $uploads
     */
    public function setUploads($uploads): void
    {
        $this->uploads = $uploads;
    }

    public function __toString()
    {
        return $this->getUserName();
    }
}
