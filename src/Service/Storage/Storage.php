<?php
/**
 * Created by PhpStorm.
 * User: DanteChaos
 * Date: 21-12-2017
 * Time: 14:29
 */

namespace App\Service\Storage;


use App\Entity\Upload;
use Doctrine\Common\Persistence\ManagerRegistry;

class Storage
{
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Saves record to database
     * @param array $post
     */
    public function addToDatabase(array $post){
        $user = $this->doctrine->getRepository('App:User')->find(1);
        $expiresAt = new \DateTime();
        $expiresAt->modify("+2 MONTH");
        $upload = new Upload();
        $upload->setFileName($post['name']);
        $upload->setFileLink($post['file']);
        $upload->setFileType($post['type']);
        $upload->setUser($user);
        $upload->setExpiresAt($expiresAt);
        $upload->setVisibility($post['visibility']);
        $this->doctrine->getManager()->persist($upload);
        $this->doctrine->getManager()->flush();
    }

    public function saveToDatabase($key,$post){

    }

    public function deleteFromDatabase($key){

    }

    /**
     * @param $user
     * @param $page
     * @param $limit
     * @return array
     */
    public function listFromDatabase($user,$page,$limit){
        return $this->doctrine->getRepository('App:Upload')->findByUser($user,$page,$limit);
    }

    /**
     * @param $id
     * @return Upload|null|object
     */
    public function detailsFromDatabase($id){
        return $this->doctrine->getRepository('App:Upload')->find($id);
    }
}