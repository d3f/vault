<?php
/**
 * Created by PhpStorm.
 * User: Estaban
 * Date: 13-12-2017
 * Time: 16:00
 */

namespace App\Service\Storage;


use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AwsS3 extends Storage implements StorageInterface {


	/**
	 * @var S3Client
	 */
	private $client;

	/**
	 * @var string
	 */
	private $bucket;
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine, $bucket, array $s3arguments ) {

		$this->setBucket( $bucket );
		$this->setClient( new S3Client( $s3arguments ) );
        $this->doctrine = $doctrine;
        parent::__construct($doctrine);
    }

	/**
	 * @param UploadedFile $uploadedFile
	 * @param string       $privacy
	 *
	 * @return string file url
	 */
	public function uploadFile( UploadedFile $uploadedFile, $privacy = 'public-read' ) {
		return $this->upload( $uploadedFile->getClientOriginalName(), file_get_contents( $uploadedFile->getPathname() ), array('params' => array('ContentType' => $uploadedFile->getMimeType())), $privacy );
	}

	/**
	 * @param string $fileName
	 * @param string $content
	 * @param array  $meta
	 * @param string $privacy
	 *
	 */
	public function upload( $fileName, $content, array $meta = [], $privacy = 'public-read' ) {
	    $this->client->upload( $this->getBucket(), $fileName, $content, $privacy, [
            'Metadata' => $meta
        ] );
	}

	/**
	 * @return string
	 */
	public function getBucket(): string {
		return $this->bucket;
	}

	/**
	 * @param string $bucket
	 */
	public function setBucket( string $bucket ): void {
		$this->bucket = $bucket;
	}

	/**
	 * @param $filename
	 *
	 * @return \Aws\Result
	 */
	public function deleteFile( $filename ) {
		return $this->client->deleteObject( [ 'Bucket' => $this->getBucket(), 'Key' => $filename ] );
	}

	/**
	 * @param int $page
	 * @param int $limit
	 *
	 * @return array|\Aws\Result|mixed|null
	 */
	public function listFiles( $page = 1, $limit = 10 ) {
		$objects = [];
		try {
			$objects = $this->client->listObjects( [ 'Bucket' => $this->getBucket() ] );
		} catch ( S3Exception $exception ) {
			echo $exception->getMessage() . "\n";
		}

		if ( ! empty( $objects ) ) {
			return $objects->get( 'Contents' );
		}

		return $objects;
	}

	/**
	 * @param $key
	 *
	 * @return bool|mixed|null
	 */
	public function fileDetails( $key ) {
		$result = [];
		$object = $this->client->getObject( [ 'Bucket' => $this->getBucket(), 'Key' => $key ] );
		if ( ! empty( $object ) ) {
			$meta   = $object->get( '@metadata' );
			$result = [ 'type' => $meta['headers']['content-type'], 'url' => $meta['effectiveUri'],'lastModified' => $meta['headers']['last-modified'] ];
		}

		return $result;
	}

	/**
	 * @return S3Client
	 */
	public function getClient(): S3Client {
		return $this->client;
	}

	/**
	 * @param S3Client $client
	 */
	public function setClient( S3Client $client ): void {
		$this->client = $client;
	}


}