<?php
/**
 * Created by PhpStorm.
 * User: Estaban
 * Date: 15-12-2017
 * Time: 15:12
 */

namespace App\Service\Storage;



use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Filesystem\Filesystem AS NativeFileSystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileSystem extends Storage implements StorageInterface {


	private $baseFolder;
	/**
	 * @var Finder
	 */
	private $finder;
	/**
	 * @var NativeFileSystem
	 */
	private $nativeFileSystem;
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    /**
     * FileSystem constructor.
     * @param ManagerRegistry $doctrine
     * @param NativeFileSystem $nativeFileSystem
     * @param $baseFolder
     */
    public function __construct(ManagerRegistry $doctrine, NativeFileSystem $nativeFileSystem , $baseFolder) {

		$this->baseFolder = $baseFolder;
		$this->finder = new Finder();
		$this->nativeFileSystem = $nativeFileSystem;
        $this->doctrine = $doctrine;
		parent::__construct($doctrine);
	}


    /**
     * @param UploadedFile $uploadedFile
     * @param string $privacy
     * @return string
     */
    public function uploadFile(UploadedFile $uploadedFile, $privacy = 'public' ) {

		$fileName = md5(uniqid()).'.'.$uploadedFile->guessExtension();
		$uploadedFile->move(
			$this->baseFolder,
			$fileName
		);
		return $fileName;
	}

    /**
     * @param $key
     */
    public function deleteFile($key ) {
		$this->nativeFileSystem->remove($this->baseFolder.$key);
	}

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function listFiles($page = 1, $limit = 10 ) {
		$list = [];
		$files = $this->finder->sortByName()->files()->in($this->baseFolder);
		if(!empty($files)){
			foreach ($files as $file){
				#TODO: Add mimetype
				$list[] = ['Key' => $file->getRelativePathname(),'url' => $file->getPathName(),'type' => '', 'LastModified' => $file->getMTime()];
			}
		}
		return $list;
	}

    /**
     * @param $key
     */
    public function fileDetails($key ) {
		// TODO: Implement fileDetails() method.
	}
}